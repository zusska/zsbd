alter system flush buffer_cache;
alter system flush shared_pool;

timing start script_time
set autotrace traceonly
spool A_read_result.out

SELECT * 
FROM events 
WHERE REGIONID IN 
  (SELECT reg 
   FROM (SELECT DEFAULTREGION as reg, count(*) 
         FROM users 
         GROUP BY defaultregion 
         HAVING count(*) = (SELECT MAX(COUNT(users.ID)) 
                            FROM users 
                            GROUP BY DEFAULTREGION)));


SELECT * 
FROM events 
  LEFT JOIN groups ON (groups.id = events.organizerid) 
WHERE groups.id IN 
  (SELECT DISTINCT groupid 
   FROM memberships 
   WHERE userid IN 
     (SELECT id 
      FROM users 
      WHERE surname LIKE 'A%'))
  AND eventdatetime > to_date('01-03-16','DD-MM-YY');


SELECT news.* 
FROM news 
WHERE eventid IN 
  (SELECT id 
   FROM events 
   WHERE regionid IN 
     (SELECT regionid 
      FROM (SELECT regionid, COUNT(*) 
            FROM events 
            GROUP BY regionid 
            ORDER BY 2 ASC)
      WHERE ROWNUM <=10))
  AND news.creationdate > to_date('01-03-00','DD-MM-YY') 
  AND news.creationdate < to_date('01-03-05','DD-MM-YY') 
ORDER BY creationdate DESC;


SELECT * 
FROM events 
  LEFT JOIN tickets ON (events.id = tickets.eventid) 
  LEFT JOIN soldtickets ON (tickets.id = soldtickets.ticketid)  
  LEFT JOIN participants ON (soldtickets.participantid = participants.id)
WHERE participants.userid IN 
  (SELECT id 
   FROM users 
   WHERE id NOT IN (SELECT userid FROM coordinators)) 
     AND events.eventdatetime >= to_date('01-03-13','DD-MM-YY') 
ORDER BY eventdatetime ASC;


SELECT id as EI 
FROM events 
WHERE regionid IN (SELECT defaultregion FROM users) 
  AND eventdatetime > to_date('01-03-16','DD-MM-YY'); 


timing stop script_time
spool off
exit



