-- niefortunna nazwa ;)
-- eventParam: jakis int bedacy parametrem zmiany, np id rekordu
CREATE TABLE EventLog
(
    entryDate timestamp default CURRENT_TIMESTAMP,
    tableName varchar(255),
    functionName varchar(255),
    message varchar(255),
    eventParam number
);

-------------------------------------------------------------------------------

-- fix:
update Events set eventdatetime = creationdate + 7 where eventdatetime <= creationdate;

-- 1a
alter table Events
add constraint event_date_creation_ct CHECK (eventdatetime > creationdate);

-- 1b
create or replace trigger event_date_change_tr
before update on Events
for each row
when (new.eventdatetime < old.eventdatetime)
begin
  raise_application_error(-20001, 'new date must be greater');
end;

-------------------------------------------------------------------------------
--2
CREATE OR REPLACE TRIGGER news_for_event_tr
AFTER INSERT ON Events
FOR EACH ROW

DECLARE
    v_authorid varchar(20);

BEGIN
    select createdby INTO v_authorid from groups where id = :new.organizerid;

    INSERT INTO news(eventid, authorid, message, creationdate)
    VALUES (:new.id, v_authorid, concat(:new.title, ' created!'), current_timestamp);

    INSERT INTO EventLog(tableName, functionName, message, eventParam) 
    VALUES ('NEWS', 'news_for_event', 'after insert on event news created', :new.id);
END;    

-------------------------------------------------------------------------------
--3
CREATE OR REPLACE TRIGGER less_sold_than_avail_tr
BEFORE UPDATE ON soldtickets
FOR EACH ROW

DECLARE
  v_avail_tickets number;
  v_sold_tickets number;

BEGIN
  select count INTO v_avail_tickets from Tickets where id = :new.ticketid;
  select count(*) into v_sold_tickets from Soldtickets where ticketid = :new.ticketid;
  
  IF (v_sold_tickets >= v_avail_tickets)
  THEN
    INSERT INTO EventLog(tableName, functionName, message) 
    VALUES ('SOLDTICKETS', 'less_sold_than_avail_tr', 'Too many sold tickets, rollback');
    rollback;  
  ELSE
    INSERT INTO EventLog(tableName, functionName, message, eventParam) 
    VALUES ('SOLDTICKETS', 'less_sold_than_avail_tr', 'Check passed, soldtickets updated', :new.id);
  END IF;
END;       

-------------------------------------------------------------------------------
--3
CREATE OR REPLACE TRIGGER less_sold_than_avail_tick_tr
BEFORE UPDATE ON Tickets
  FOR EACH ROW
  
  DECLARE
  v_sold_tickets number;
  
  BEGIN
    select count(*) into v_sold_tickets from Soldtickets where ticketid = :new.id;
  
    IF (v_sold_tickets >= :new.count)
    THEN
      INSERT INTO EventLog(tableName, functionName, message, eventParam) 
      VALUES ('TICKETS', 'less_sold_than_avail_tick_tr', 'Too many sold tickets, rollback', :new.ticketid);
      rollback;  
    ELSE
      INSERT INTO EventLog(tableName, functionName, message, eventParam) 
      VALUES ('TICKETS', 'less_sold_than_avail_tick_tr', 'Check passed, tickets updated', :new.id);
    END IF;
END;

----------------
-- 4

CREATE OR REPLACE TRIGGER cant_buy_past_events_tr
BEFORE UPDATE ON soldtickets 
FOR EACH ROW

DECLARE
  event_date timestamp;

BEGIN
  select eventdatetime INTO event_date from Events, Tickets where Tickets.id = :new.ticketid 
  and Events.id = Tickets.eventid;
  
  IF (current_date <= event_date)
  THEN
    INSERT INTO EventLog(tableName, functionName, message, eventParam) 
    VALUES ('SOLDTICKETS', 'cant_buy_past_events_tr', 'Cannot buy ticket for past event, rollback', :new.ticketid);
    rollback;  
  ELSE
    INSERT INTO EventLog(tableName, functionName, message, eventParam) 
    VALUES ('SOLDTICKETS', 'cant_buy_past_events_tr', 'Bought ticket for an event', :new.id);
  END IF;
END; 
-------------------------------------------------------------------------------
--5

alter table Users add constraint users_hash_not_null check (PasswordHash is not null);
alter table Users add constraint users_salt_not_null check (PasswordSalt is not null);

CREATE OR REPLACE TRIGGER users_password_change_trigger
BEFORE UPDATE ON Users
FOR EACH ROW
WHEN 
  ((new.PasswordHash is not null AND new.PasswordHash <> old.PasswordHash) OR 
   (new.PasswordSalt is not null AND old.PasswordSalt <> old.PasswordSalt))
BEGIN
  if (:new.PasswordHash <> :old.PasswordHash and
      :new.PasswordSalt <> :old.PasswordSalt) then
      insert into EventLog(tableName, functionName, message, eventParam)
      values ('USERS', 'users_password_change_trigger', 'updated successfully', :new.id);
  else
      --insert into EventLog(tableName, functionName, message)
      --values ('USERS', 'users_password_change_trigger', 'one of hash, salt has not changed, rollback');
      --raise_application_error(-20042, 'rollback');
      rollback;
  end if;
END;

--------------------------------------------------------------------------------
--6
create table PriceLog (
  entryDate timestamp default current_timestamp,
  ticketId number not null,
  oldPrice number,
  newPrice number
);  

create or replace trigger pricelog_log_pricelog_entry
after insert on PriceLog
for each row
begin
  insert into EventLog (tableName, functionName, message, eventParam)  
    values ('PRICELOG', 'pricelog_log_pricelog_entry', 'price change log saved', :new.ticketid);
end;

create or replace trigger tickets_log_price_change_trg
after update of defaultprice on tickets
for each row
begin
  insert into PriceLog (ticketId, oldPrice, newPrice)
    values (:new.id, :old.defaultprice, :new.defaultprice);
  insert into EventLog (tableName, functionName, message, eventParam)
    values ('TICKETS', 'tickets_log_price_change_trigger_trg', 'price successfully changed', :new.id);
end;

create or replace trigger tickets_max_pricerise_trg
before update of defaultPrice on Tickets
for each row
when (new.defaultPrice > 1.2 * old.defaultPrice)
begin
  raise_application_error(-20042, 'price increase too high');
  rollback;
end;

alter table Tickets add constraint tickets_defprice_gte_zero check(defaultPrice >= 0);

--------------------------------------------------------------------------------
-- 7
create or replace trigger coordinators_group_match_trg
before insert or update on coordinators
for each row
declare
  group_id number;
  matches number;
begin
  select organizerid into group_id
    from Events 
    where id = :new.eventid;
  select count(*) into matches
    from Memberships
    where groupId = group_id and userid = :new.userid;
  if (matches < 1) then
    raise_application_error(-20042, 'coordinator from outside the organizing group');
    rollback;
  end if;
  insert into EventLog(tableName, functionName, message, eventParam)
    values ('COORDINATORS', 'coordinators_group_match_trg', 'coordinator successfully changed', :new.userid);
end;

--------------------------------------------------------------------------------
-- 8
create or replace procedure RemoveItemsDependingOnEvent(event_id in number) 
as
begin
  delete from News where EventId = event_id;
  delete from Coordinators where EventId = event_id;
  delete from Soldtickets where TicketId in
    (select id from Tickets where EventId = event_id);
  delete from Participants where EventId = event_id;
  delete from Tickets where EventId = event_id;
end;

create or replace trigger events_bef_delete_trg
before delete on Events
for each row
begin
  RemoveItemsDependingOnEvent(:old.id);
  insert into EventLog (tableName, functionName, message, eventParam)
    values ('EVENTS', 'RemoveItemsDependingOnEvent', 'removed dependants', :old.id);
end;

create or replace trigger events_aft_delete_trg
after delete on Events
for each row
begin
  insert into EventLog (tableName, functionName, message, eventParam)
    values ('EVENTS', 'events_aft_delete_trg', 'removed event', :old.id);
end;

create or replace trigger events_cancel_trg
after update of Cancelled on Events
for each row
begin
  if (:old.Cancelled = 1 and :new.Cancelled = 0) then
    raise_application_error(-20042, 'event already cancelled');
    rollback;
  end if;
  if (:new.Cancelled = 1) then
    RemoveItemsDependingOnEvent(:new.id);
    insert into EventLog (tableName, functionName, message, eventParam)
      values ('EVENTS', 'RemoveItemsDependingOnEvent', 'removed dependants', :old.id);
    insert into EventLog (tableName, functionName, message, eventParam)
      values ('EVENTS', 'events_cancel_trg', 'event cancelled', :new.id);  
  end if;
end;

--------------------------------------------------------------------------------
